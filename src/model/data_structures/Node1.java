package model.data_structures;

import java.util.ArrayList;

public class Node1<T , V>
	{
		 private ArrayList<V> valores;
		
		 private T key;
		
		 Node1<T,V> siguiente;
		
		
		public Node1(T dato , V v )
		{
			key= dato;
			valores = new ArrayList<V>();
			valores.add(v);
			siguiente=null;
		}
		
		public Node1<T,V> darSiguiente()
		{
			return siguiente;
		}
		
		public ArrayList<V> darValores()
		{
			return valores;
		}
		
		public void cambiarSig(Node1<T,V> x)
		{
			siguiente=x;
		}
		
		public T darKey()
		{
			return key;
		}
		
	}	

