package model.data_structures;

import java.util.ArrayList;
import java.util.Iterator;

public class SeparateChainingHash <Key extends Comparable<Key> ,Value>
{
	//Numero de keys
	private int n;
	//Tama�o del arreglo
	private int m;
	//Arreglo de keys
	
	private int numeroRehashes;
	
	private Node1<Key, Value>[] arreglo;
	
	public SeparateChainingHash(int mm) 
	{
        m = mm;
        arreglo = (Node1<Key, Value>[]) new Node1[m];
    }
	
	public int darNumeroTuplas()
	{
		return n;
	}
	
	public int darTama�o()
	{
		return m;
	}
	
	public int darNumeroRehashes()
	{
		return numeroRehashes;
	}
	
	public Node1<Key, Value>[] darArreglo()
	{
		return arreglo;
	}
	
	public int hash(Key key)
	{ 
		return (key.hashCode() & 0x7fffffff) % m; 
	}
	
	public void putInSet(Key key, Value val) 
	{
		 int i = hash(key);
		 for (Node1<Key, Value> x = arreglo[i]; x != null; x = x.darSiguiente())
		 {
			 if (key.equals(x.darKey())) 
			 { 
				 x.darValores().add(val);
				 return;
			 }
		 }
		 Node1<Key, Value> y = arreglo[i];
		 if(y==null)
		 {
			 arreglo[i] = new Node1<Key, Value>(key,val);
			 n++;
			 return;
		 }
		 while(y.darSiguiente()!=null)
		 {
			 y = y.darSiguiente();
		 }
		 y.cambiarSig(new Node1<Key, Value>(key, val));
		 n++;
		 if(n/m > 5)
		 {
			 resize(2*m);
		 }
	} 
	
	private void resize(int chains) 
	{
		numeroRehashes++;
        SeparateChainingHash<Key, Value> temp = new SeparateChainingHash<Key, Value>(chains);
        for (int i = 0; i < m; i++) 
        {
        	Node1<Key, Value> x = arreglo[i];
        	while(x!=null)
        	{
        		ArrayList<Value> valores = x.darValores();
        		for(int p = 0; i<valores.size(); i++)
        		{
        			temp.putInSet(x.darKey(), valores.get(p));
        		}
        		x = x.darSiguiente();
        	}
        }
        this.m  = temp.m;
        this.n  = temp.n;
        this.arreglo = temp.arreglo;
    }
	
	public Iterator<Value> getSet(Key k)
	{
		int i = hash(k);
		Node1<Key, Value> x = arreglo[i];
		while(x!=null)
		{
			if(k == x.darKey())
			{
				return x.darValores().iterator();
			}
			x = x.darSiguiente();
		}
		ArrayList<Value> listaVacia = new ArrayList<Value>();
		return listaVacia.iterator();
	}
	
	public Iterator<Value> deleteSet(Key k)
	{
		int i = hash(k);
		Node1<Key, Value> x = arreglo[i];
		Node1<Key, Value> anterior = arreglo[i];
		if(x!=null && x.darKey()==k)
		{
			return x.darValores().iterator();
		}
		else
		{
			x = x.darSiguiente();
			while(x!=null)
			{
				if(k == x.darKey())
				{
					anterior.cambiarSig(x.darSiguiente());
					return x.darValores().iterator();
				}
				anterior = anterior.darSiguiente();
				x = x.darSiguiente();
			}
		}
		ArrayList<Value> listaVacia = new ArrayList<Value>();
		return listaVacia.iterator();
	}
	
	public Iterator<Key> keys() 
	{
        ArrayList<Key> lista = new ArrayList<Key>();
        for (int i = 0; i < m; i++) 
        {
        	Node1<Key, Value> x = arreglo[i];
        	while(x!=null)
        	{
        		lista.add((Key) x.darKey());
        		x = x.darSiguiente();
        	}
        }
        return lista.iterator();
    }
}
