package model.logic;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import com.opencsv.CSVReader;

import model.data_structures.Cola;
import model.data_structures.HashLinearProbing;
import model.data_structures.SeparateChainingHash;

/**
 * Definicion del modelo del mundo
 *
 */
public class MVCModelo {

	private HashLinearProbing<String, TravelTime> hashlinear;
	private SeparateChainingHash separate;
	
	public MVCModelo()
	{

		separate = new SeparateChainingHash(100);
		hashlinear=new HashLinearProbing<String, TravelTime>(839);
	}
	
	public void loadTravelTimes()
	{
		CSVReader reader = null;
		CSVReader reader2=null;
		CSVReader reader3=null;
		CSVReader reader4=null;

		try {

			reader = new CSVReader(new FileReader("./data/bogota-cadastral-2018-1-WeeklyAggregate.csv"));
			reader2 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-2-WeeklyAggregate.csv"));
			reader3 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-3-WeeklyAggregate.csv"));
			reader4 = new CSVReader(new FileReader("./data/bogota-cadastral-2018-4-WeeklyAggregate.csv"));
			
			try{
				String[] x =reader.readNext();
				String[] y =reader2.readNext();
				String[] z =reader3.readNext();
				String[] r =reader4.readNext();

			}
			catch(Exception e)
			{
				System.out.println("Problemas leyendo la primera linea");
			}
			
			int cont=0;
			TravelTime viaje=null;
			for(String[] nextLine : reader) 
			{
				TravelTime dato = new TravelTime( 1 ,Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				String llave= "1-"+dato.darSourceid()+"-"+ dato.darDtid();
				hashlinear.putInSet(llave, dato);
				separate.putInSet(llave, dato);
				viaje=dato;
				cont++;
				if(cont==1)
				{
					System.out.println("Datos primer viaje archivo 1: "+ "Zona Origen: "+ dato.darSourceid()+", Zona destino: "+dato.darDtid()+", D�a: "+dato.darDow()+" ,Tiempo promedio: "+dato.darMean_travel_time());
				}
			}
			System.out.println("Datos �ltimo viaje archivo 1: "+ "Zona Origen: "+ viaje.darSourceid()+", Zona destino: "+viaje.darDtid()+", D�a: "+viaje.darDow()+" ,Tiempo promedio: "+viaje.darMean_travel_time());

			int cont1=0;
			TravelTime viaje1=null;
			for(String[] nextLine : reader2) 
			{
				TravelTime dato = new TravelTime(2, Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				String llave= "2-"+dato.darSourceid()+"-"+ dato.darDtid();
				hashlinear.putInSet(llave, dato);
				separate.putInSet(llave, dato);
				viaje1=dato;
				cont1++;
				if(cont1==1)
				{
					System.out.println("Datos primer viaje archivo 2: "+ "Zona Origen: "+ dato.darSourceid()+", Zona destino: "+dato.darDtid()+", D�a: "+dato.darDow()+" ,Tiempo promedio: "+dato.darMean_travel_time());
				}
			}
			System.out.println("Datos �ltimo viaje archivo 2: "+ "Zona Origen: "+ viaje1.darSourceid()+", Zona destino: "+viaje1.darDtid()+", D�a: "+viaje1.darDow()+" ,Tiempo promedio: "+viaje1.darMean_travel_time());

			int cont2=0;
			TravelTime viaje2=null;
			for(String[] nextLine : reader3) 
			{
				TravelTime dato = new TravelTime( 3, Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				String llave= "3-"+dato.darSourceid()+"-"+ dato.darDtid();
				hashlinear.putInSet(llave, dato);
				separate.putInSet(llave, dato);
				cont2++;
				viaje2=dato;
				if(cont2==1)
				{
					System.out.println("Datos primer viaje archivo 3: "+ "Zona Origen: "+ dato.darSourceid()+", Zona destino: "+dato.darDtid()+", D�a: "+dato.darDow()+" ,Tiempo promedio: "+dato.darMean_travel_time());
				}
			}
			System.out.println("Datos �ltimo viaje archivo 3: "+ "Zona Origen: "+ viaje2.darSourceid()+", Zona destino: "+viaje2.darDtid()+", D�a: "+viaje2.darDow()+" ,Tiempo promedio: "+viaje2.darMean_travel_time());

			int cont3=0;
			TravelTime viaje3=null;
			for(String[] nextLine : reader4) 
			{
				TravelTime dato = new TravelTime( 4, Integer.parseInt(nextLine[0]), Integer.parseInt(nextLine[1]), Integer.parseInt(nextLine[2]), Double.parseDouble(nextLine[3]), Double.parseDouble(nextLine[4]), Double.parseDouble(nextLine[5]), Double.parseDouble(nextLine[6]));
				String llave= "4-"+dato.darSourceid()+"-"+ dato.darDtid();
				hashlinear.putInSet(llave, dato);
				separate.putInSet(llave, dato);
				cont3++;
				viaje3=dato;
				if(cont3==1)
				{
					System.out.println("Datos primer viaje archivo 4: "+ "Zona Origen: "+ dato.darSourceid()+", Zona destino: "+dato.darDtid()+", D�a: "+dato.darDow()+" ,Tiempo promedio: "+dato.darMean_travel_time());
				}
			}
			System.out.println("Datos �ltimo viaje archivo 4: "+ "Zona Origen: "+ viaje3.darSourceid()+", Zona destino: "+viaje3.darDtid()+", D�a: "+viaje3.darDow()+" ,Tiempo promedio: "+viaje3.darMean_travel_time());
		}
		catch (FileNotFoundException e) 
		{
			e.printStackTrace();
		} 
		finally
		{
			if (reader != null && reader2!=null ) 
			{
				try 
				{
					reader.close();
					reader2.close();
					reader3.close();
					reader4.close();

				} catch (IOException e) 
				{
					e.printStackTrace();
				}
			}
		}
	}
	
	public HashLinearProbing<String, TravelTime> darlinel()
	{
		return hashlinear;
	}

	public void buscarLinear(int trim, int or, int des)
	{
		String llave= trim+"-"+or+"-"+des;
		Iterator<TravelTime> f=hashlinear.getSet(llave);
		while (f.hasNext())
		{
			TravelTime vij= f.next();
			System.out.println("Informaci�n viaje: Trimestre: "+vij.darTrim()+", Zona de origen: "+vij.darSourceid()+", Zona de llegada: "+vij.darDtid()+", D�a: "+vij.darDow()+", Tiempo promedio: "+ vij.darMean_travel_time());
		}
	}
	
	public ArrayList<Long> testlinear()
	{
		Iterator<String> iter=hashlinear.keys();
		ArrayList<Long> lista= new ArrayList<Long>();
		int ex=0;
		int noex=0;
		
		while(iter.hasNext() && ex<8000)
		{
			long tiempo= System.currentTimeMillis();
			hashlinear.getSet(iter.next());
			long tiemp=System.currentTimeMillis()-tiempo;
			lista.add(tiemp);
			ex++;
		}
		
		int rnd = (int) (Math.random() * 10) + 6;
		int un = (int) (Math.random() * 999) + 100;
		int dos = (int) (Math.random() *999) +100;
		String llaves=rnd+"-"+un+"-"+dos;
		while(noex<2000)
		{
			long tiempo= System.currentTimeMillis();
			hashlinear.getSet(llaves);
			long tiemp=System.currentTimeMillis()-tiempo;
			lista.add(tiemp);
			noex++;
		}
		return lista;
	}
	public SeparateChainingHash darSeparate()
	{
		return separate;
	}
	
	public void requerimiento2(int trim, int sour, int dest)
	{
		String llave = trim + "-" + sour + "-" + dest;
		Iterator<TravelTime> iter = separate.getSet(llave);
		if(iter.hasNext()==false)
		{
			System.out.println("No hay datos con los parametros dados");
		}
		else
		{
		for(int i=0; i<8;i++)
		{
			Iterator<TravelTime> itera = separate.getSet(llave);
			while(itera.hasNext())
			{
				TravelTime viaje = (TravelTime)itera.next();
				if(viaje.darDow()==i)
				{
					System.out.println(viaje.darTrim() + " " + viaje.darSourceid() + " " + viaje.darDtid() + " "+ viaje.darDow() + " " + viaje.darMean_travel_time());
				}
			}
		}
		}
	}
	
	public ArrayList<Long> pruebasSeparateChaining()
	{
		Iterator iter = separate.keys();
		ArrayList<Long> lista = new ArrayList<Long>();
		int cont1 = 0;
		while(iter.hasNext() && cont1<8000)
		{
			long t1 = System.currentTimeMillis();
			separate.getSet((Comparable) iter.next());
			long t2 = System.currentTimeMillis() - t1;
			lista.add(t2);
			cont1++;
		}
		int rnd = (int) (Math.random()*10)+6;
		int un = (int) (Math.random()*999)+100;
		int dos = (int) (Math.random()*999)+100;
		String llave = rnd+"-"+un+"-"+dos;
		int cont2 = 0;
		while(cont2<2000)
		{
			long t1 = System.currentTimeMillis();
			separate.getSet(llave);
			long t2 = System.currentTimeMillis() - t1;
			lista.add(t2);
			cont2++;
		}
		return lista;
	}
	
	}
	
	
	

