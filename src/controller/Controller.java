package controller;

import java.util.ArrayList;
import java.util.Scanner;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.SystemMenuBar;

import model.logic.MVCModelo;
import view.MVCView;

public class Controller {

	/* Instancia del Modelo*/
	private MVCModelo modelo;
	
	/* Instancia de la Vista*/
	private MVCView view;
	
	/**
	 * Crear la vista y el modelo del proyecto
	 * @param capacidad tamaNo inicial del arreglo
	 */
	public Controller ()
	{
		view = new MVCView();
		modelo = new MVCModelo();
	}
		
	public void run() 
	{
		Scanner lector = new Scanner(System.in);
		boolean fin = false;
		String dato = "";
		String respuesta = "";

		while( !fin ){
			view.printMenu();

			int option = lector.nextInt();
			switch(option){
			case 1:
				modelo.loadTravelTimes();
				System.out.println("N�mero de duplas en tabla hash LinearProbing: "+modelo.darlinel().darDatos());
				System.out.println("Tama�o final del arreglo  de tabla hash LinearProbing: "+modelo.darlinel().darcapacidad());
				System.out.println("Factor de carga en tabla hash LinearProbing: "+modelo.darlinel().darFactorCarga());
				System.out.println("N�mero de rehashes en tabla hash LinearProbing: "+modelo.darlinel().darRehashes());
				
				System.out.println("El numero de tuplas en SeparateChaining es de " + modelo.darSeparate().darNumeroTuplas());
				
				System.out.println("El tama�o inicial en SeparateChaining fue de 100");
				
				System.out.println("El tama�o final en SeparateChaining es de " + modelo.darSeparate().darTama�o());
				
				System.out.println("El factor de carga en SeparateChaining fue de " + (modelo.darSeparate().darNumeroTuplas() / modelo.darSeparate().darTama�o()));
				
				System.out.println("El numero de rehashes en SeparateChaining que tuvo que hacer fue de " + modelo.darSeparate().darNumeroRehashes());

				break;

			case 2:
				System.out.println("Ingresar trimestre:");
				dato=lector.next();
				int trim=Integer.parseInt(dato);
				System.out.println("Ingresar origen:");
				dato=lector.next();
				int or=Integer.parseInt(dato);
				System.out.println("Ingresar destino:");
				dato=lector.next();
				int des=Integer.parseInt(dato);
				modelo.buscarLinear(trim, or, des);
				break;

			case 3:
				System.out.println("Ingrese el trimestre");
				int trimm = Integer.parseInt(lector.next());
				System.out.println("Ingrese el source");
				int sour = Integer.parseInt(lector.next());
				System.out.println("Ingrese el destino");
				int dest = Integer.parseInt(lector.next());
				
				modelo.requerimiento2(trimm, sour, dest);
				break;
			
			case 4:
				ArrayList<Long> arr=modelo.testlinear();
				long menor=arr.get(0);
				long mayor= arr.get(0);
				long sume=0;
				for(int i=0; i<arr.size();i++)
				{
					sume+=arr.get(i);
					if(arr.get(i)<menor)
						menor=arr.get(i);
					if(arr.get(i)>mayor)
						mayor=arr.get(i);
				}
				double prom=(double)sume/arr.size();
				System.out.println("El mayor tiempo en pedir un dato en milisegundos es: "+mayor);
				System.out.println("El menor tiempo en pedir un dato en milisegundos es: "+menor);
				System.out.println("El tiempo promedio en pedir un dato en milisegundos es:"+prom);


				break;
				
			case 5:
				ArrayList<Long> lista = modelo.pruebasSeparateChaining();
				long mayorr = 0;
				long menorr = 99999999;
				long suma = 0;
				for(int i=0; i<lista.size();i++)
				{
					if(lista.get(i) > mayorr)
					{
						mayorr = lista.get(i);
					}
					if(lista.get(i) < menorr)
					{
						menorr = lista.get(i);
					}
					suma = suma + lista.get(i);
				}
				System.out.println("El mayor tiempo fue de "+mayorr);
				System.out.println("El menor tiempo fue de "+menorr);
				System.out.println("El tiempo promedio fue de "+  (suma / lista.size()));
				break;
				
			default: 
					System.out.println("--------- \n Opcion Invalida !! \n---------");
					break;
			}
		}
		
	}	
}
